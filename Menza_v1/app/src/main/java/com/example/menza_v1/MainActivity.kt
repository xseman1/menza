package com.example.menza_v1

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*

class MainActivity : AppCompatActivity() {
    private lateinit var database : FirebaseDatabase
    private lateinit var ref : DatabaseReference
    private lateinit var adapter : RecyclerAdapter
    private val listMenz = mutableListOf<Menza>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadData()

        database = FirebaseDatabase.getInstance()
        ref = database.getReference("menzy")
        adapter = RecyclerAdapter(this)

        findViewById<RecyclerView>(R.id.zoznamMenz).layoutManager = LinearLayoutManager(this)
        findViewById<RecyclerView>(R.id.zoznamMenz).adapter = adapter

        ref.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                listMenz.clear()
                for (data in snapshot.children) {
                    listMenz.add(Menza(data.key.toString(), data.child("nazov").getValue().toString(), data.child("status").getValue().toString()))
                }
                adapter.setListData(listMenz)
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun loadData() {
        val sharedPreferences = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE)
        val savedId = sharedPreferences.getString("ID_KEY", null)

        if(!savedId.isNullOrBlank()) {
            var intent = Intent(this, MenzaActivity::class.java).apply {
                putExtra(AlarmClock.EXTRA_MESSAGE, savedId)
            }
            ContextCompat.startActivity(this, intent, null)
        }
    }
}
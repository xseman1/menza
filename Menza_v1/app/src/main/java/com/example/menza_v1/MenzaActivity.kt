package com.example.menza_v1

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.*

class MenzaActivity : AppCompatActivity() {
    private lateinit var nodeMenzy : DatabaseReference
    private lateinit var nodeAdresy : DatabaseReference
    private var message: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menza)

        message = intent.getStringExtra(AlarmClock.EXTRA_MESSAGE)

        saveData()

        nodeMenzy = FirebaseDatabase.getInstance().getReference("menzy")
        nodeAdresy = FirebaseDatabase.getInstance().getReference("adresy")

        nodeMenzy.addValueEventListener(getDataMenzy())
        nodeAdresy.addValueEventListener(getDataAdresy())

        findViewById<TextView>(R.id.zmena_statusu).setOnClickListener {
            Toast.makeText(this, "Kliknutie na zmenu statusu", Toast.LENGTH_SHORT).show()
            changeOfStatus()
        }
    }

    private fun getDataMenzy() : ValueEventListener {
        var data = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (data in snapshot.children) {
                    if (data.key.equals(message))  {
                        findViewById<TextView>(R.id.nazovMenzy).setText(data.child("nazov").getValue().toString())
                        findViewById<TextView>(R.id.status).setText(data.child("status").getValue().toString())
                    }
                }

            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        }

        return data
    }

    private fun getDataAdresy() : ValueEventListener {
        var data = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (data in snapshot.children) {
                    if (data.key.equals(message))  {
                        findViewById<TextView>(R.id.adresa).setText(data.child("adresa").getValue().toString())
                    }
                }

            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        }

        return data
    }

    private fun changeOfStatus() {
        var map = mutableMapOf<String, Any>()

        nodeMenzy.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (data in snapshot.children) {
                    if (data.key.equals(message))  {
                        if (data.child("status").value.toString().equals("zatvorena")) {
                            map["status"] = "otvorena"
                        }
                        else {
                            map["status"] = "zatvorena"

                        }
                        nodeMenzy.child(message.toString()).updateChildren(map)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun saveData() {
        val sharedPreferences = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply {
            putString("ID_KEY", message)
            commit()
        }

        Toast.makeText(this, "ID menzy # ${message} # ulozene", Toast.LENGTH_SHORT).show()
    }
}
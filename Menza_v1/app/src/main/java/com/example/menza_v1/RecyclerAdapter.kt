package com.example.menza_v1

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.provider.AlarmClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter(private val context : Context) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {
    private var dataList = mutableListOf<Menza>()

    fun setListData(data : MutableList<Menza>) {
        dataList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item_row, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val menza = dataList[position]
        holder.bindView(menza)
    }

    override fun getItemCount(): Int {
        if (dataList.size > 0)
            return dataList.size
        else
            return 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var nazov = itemView.findViewById<TextView>(R.id.nazov)

        init {
            itemView.setOnClickListener{
//                Toast.makeText(itemView.context, "Vybral si ${nazov.hint}", Toast.LENGTH_SHORT).show()

                val message = nazov.hint
                var intent = Intent(itemView.context, MenzaActivity::class.java).apply {
                    putExtra(AlarmClock.EXTRA_MESSAGE, message)
                }
                startActivity(itemView.context, intent, null)
            }
        }

        fun bindView(menza: Menza) {
            nazov.text = menza.nazov
            nazov.hint = menza.key
        }
    }
}